#ifndef MATEMATICAS_H
#define MATEMATICAS_H

class Matematicas {
private:
    double evaluarFuncion(double x);

public:
    void tabularFuncion(double inicio, double fin);
};

#endif //MATEMATICAS_H
