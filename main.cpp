
#include <iostream>
#include <cmath>
using namespace std;

class Matematicas {
private:
    // Método privado para evaluar la funcion
    double evaluarFuncion(double x) {
        // Funcion (se puede cambiar por otra)
        return (2 * x) + 1;
    }

public:
    // Método público para tabular la función en un rango
    void tabularFuncion(double inicio, double fin) {
        cout << "Tabla de valores tabulados:" << endl;
        cout << "x\t| f(x)" << endl;
        cout << "-----------------" << endl;

        for (double x = inicio; x <= fin; x++) {
            double resultado = evaluarFuncion(x);
            cout << x << "\t| " << resultado << endl;
        }
    }
};

int main() {
    Matematicas mat;

    // Solicitar al usuario el rango para tabular la función
    double inicio, fin;
    cout << "Ingrese el inicio del rango: ";
    cin >> inicio;
    cout << "Ingrese el fin del rango: ";
    cin >> fin;

    // Llamar al método para tabular la función con el rango 
    mat.tabularFuncion(inicio, fin);

    return 0;
}

