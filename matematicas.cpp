#include "Matematicas.h"
#include <iostream>
using namespace std;

double Matematicas::evaluarFuncion(double x) {
    // Puedes cambiar la funcion
    return x * x + 2 * x + 1;
}

void Matematicas::tabularFuncion(double inicio, double fin) {
    cout << "Tabla de valores tabulados:" << endl;
    cout << "x\t| f(x)" << endl;
    cout << "-----------------" << endl;

    // Incremento de 0.5 en 0.5
    for (double x = inicio; x <= fin; x += 0.5) { 
        double resultado = evaluarFuncion(x);
        cout << x << "\t| " << resultado << endl;
    }
}
