// tablamultiplicar.cpp

#include "TablaMultiplicar.h"
#include <iostream>
using namespace std;

void TablaMultiplicar::generarTabla(int numero) {
    cout << "Tabla de multiplicar del " << numero << ":\n";
    for (int i = 1; i <= 10; ++i) {
        cout << numero << " x " << i << " = " << (numero * i) << "\n";
    }
}

