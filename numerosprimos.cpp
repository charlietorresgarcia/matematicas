#include "NumerosPrimos.h"
#include <iostream>

using namespace std;

bool NumerosPrimos::esPrimo(int numero) {
    if (numero <= 1) {
        return false;
    }

    for (int i = 2; i * i <= numero; ++i) {
        if (numero % i == 0) {
            return false;
        }
    }

    return true;
}

void NumerosPrimos::mostrarPrimos(int inicio, int fin) {
    if (inicio > fin) {
        cout << "El rango proporcionado no es v�lido.\n";
        return;
    }

    bool primerPrimo = true;

    for (int i = inicio; i <= fin; ++i) {
        if (esPrimo(i)) {
            if (!primerPrimo) {
                cout << ", ";
            }
            cout << i;
            primerPrimo = false;
        }
    }

    if (primerPrimo) {
        cout << "No hay n�meros primos en el rango proporcionado.\n";
    } else {
        cout << "\n";
    }
}

