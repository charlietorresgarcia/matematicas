#include <iostream>
using namespace std;

class NumerosPrimos {
public:
    // Método para ver si un número es primo
    bool esPrimo(int numero) {
        if (numero <= 1) {
            return false;
        }

        for (int i = 2; i * i <= numero; ++i) {
            if (numero % i == 0) {
                return false;
            }
        }

        return true;
    }

    // Método para mostrar números primos en un rango dado
    void mostrarPrimos(int inicio, int fin) {
        if (inicio > fin) {
            cout << "El rango proporcionado no es válido.\n";
            return;
        }

        bool primerPrimo = true;

        for (int i = inicio; i <= fin; ++i) {
            if (esPrimo(i)) {
                if (!primerPrimo) {
                    cout << ", ";
                }
                cout << i;
                primerPrimo = false;
            }
        }

        if (primerPrimo) {
            cout << "No hay números primos en el rango proporcionado.\n";
        } else {
            cout << "\n";
        }
    }
};

int main() {
    NumerosPrimos numerosPrimos;
    int opcion, inicio, fin;

    do {
        // Menú de opciones
        cout << "Menú:\n";
        cout << "1. Verificar si un número es primo\n";
        cout << "2. Mostrar números primos en un rango\n";
        cout << "3. Salir\n";
        cout << "Seleccione una opción: ";
        cin >> opcion;

        switch (opcion) {
            case 1:
                // Verificar si un número es primo
                cout << "Ingrese un número: ";
                cin >> inicio;
                if (numerosPrimos.esPrimo(inicio)) {
                    cout << inicio << " es un número primo.\n";
                } else {
                    cout << inicio << " no es un número primo.\n";
                }
                break;

            case 2:
                // Mostrar números primos en un rango
                cout << "Ingrese el primer número del rango: ";
                cin >> inicio;

                cout << "Ingrese el último número del rango: ";
                cin >> fin;

                numerosPrimos.mostrarPrimos(inicio, fin);
                break;

            case 3:
                // Salir del programa
                cout << "Saliendo del programa.\n";
                break;

            default:
                cout << "Opción no válida.\n";
                break;
        }
    } while (opcion != 3);

    return 0;
}
