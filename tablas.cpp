#include <iostream>
using namespace std;

class Tabla {
public:
    void generarTabla(int numero) {
        cout << "Tabla del " << numero << "\n";
        for (int i = 1; i <= 10; ++i) {
            cout << i << " x " << numero << " = " << (i * numero) << "\n";
        }
    }
};

int main() {
    int numero;

    // Solicitar al usuario que ingrese el número para mostrar la tabla de multiplicar
    cout << "Ingrese un numero para mostrar su tabla de multiplicar: ";
    cin >> numero;

    // Crear un objeto de la clase Tabla
    Tabla tabla;

    // Generar y mostrar la tabla de multiplicar
    tabla.generarTabla(numero);

    return 0;
}
