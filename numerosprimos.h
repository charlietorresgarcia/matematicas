#ifndef NUMEROSPRIMOS_H
#define NUMEROSPRIMOS_H

class NumerosPrimos {
public:
    // M�todo para ver si un n�mero es primo
    bool esPrimo(int numero);

    // M�todo para mostrar n�meros primos en un rango dado
    void mostrarPrimos(int inicio, int fin);
};

#endif  // NUMEROSPRIMOS_H

